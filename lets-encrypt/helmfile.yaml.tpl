releases:
  - name: lets-encrypt
    namespace: cert-manager
    chart: ./chart
    values:
      - LetsEncrypt:
          Email: "{{ .Values.services.lets_encrypt.email }}"
          Server: "{{ .Values.services.lets_encrypt.server }}"
        Cloudflare:
          Key: "{{ .Values.cloudflare.key }}"
          Email: "{{ .Values.cloudflare.email }}"
        Cluster:
          Name: "{{ .Values.cluster.name }}"
          Domain: "{{ .Values.cluster.domain }}"
          TLSSecret: "{{ .Values.cluster.tls_wildcard_secret }}"
          AdditionalDomains:
{{ .Values.cluster.additional_domains | toYaml | indent 12 }}
