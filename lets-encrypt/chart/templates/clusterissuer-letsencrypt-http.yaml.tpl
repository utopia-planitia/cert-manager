apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-http
spec:
  acme:
    email: "{{ .Values.LetsEncrypt.Email }}"
    server: "{{ .Values.LetsEncrypt.Server }}"
    privateKeySecretRef:
      name: letsencrypt-http
    solvers:
      - http01:
          ingress:
            class: nginx
