apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: '{{ lower .Values.Cluster.Name | replace "_" "-" }}-wildcard'
spec:
  secretName: "{{ .Values.Cluster.TLSSecret }}"
  secretTemplate:
    annotations:
      replicator.v1.mittwald.de/replicate-to: ".*"
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
  commonName: "*.{{ .Values.Cluster.Domain }}"
  dnsNames:
    - "*.{{ .Values.Cluster.Domain }}"
{{- range $domain := .Values.Cluster.AdditionalDomains }}
    - "{{ $domain }}"
{{- end }}
