apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    email: "{{ .Values.LetsEncrypt.Email }}"
    server: "{{ .Values.LetsEncrypt.Server }}"
    privateKeySecretRef:
      name: letsencrypt
    solvers:
      - dns01:
          cloudflare:
            email: "{{ .Values.Cloudflare.Email }}"
            apiKeySecretRef:
              name: cloudflare-api-key
              key: cloudflare-api-key
          cnameStrategy: Follow
