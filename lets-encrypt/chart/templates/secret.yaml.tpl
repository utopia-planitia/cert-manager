apiVersion: v1
kind: Secret
metadata:
  name: cloudflare-api-key
type: Opaque
stringData:
  cloudflare-api-key: "{{ .Values.Cloudflare.Key }}"
