# Cert Manager

A deployment of Cert Manager.

Cert manager automates the creation or request of certificates.

It is set up to request certificates from Let's Encrypt.  
By default a wildcard certificate is created and copied in each namespace.

It is set up to create certificates based on a pre generated ca.

## Usage

[Configuration](https://cert-manager.io/docs/configuration/) and [Useage](https://cert-manager.io/docs/usage/) are documented by the cert-manager authors.

To set up the CA issuer [Generate a signing key pair](https://docs.cert-manager.io/en/release-0.11/tasks/issuers/setup-ca.html#optional-generate-a-signing-key-pair).

The cluster ca is available via https://ca.{{ cluster-domain }}/.
