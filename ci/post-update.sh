#!/bin/bash
set -euo pipefail

CHART_VERSION=$(yq -r '.releases[] | select( .chart == "jetstack/cert-manager" ) | .version' cert-manager/helmfile.yaml | sort | uniq)

echo "CHART_VERSION: ${CHART_VERSION}"

curl -fsSL https://github.com/jetstack/cert-manager/releases/download/${CHART_VERSION}/cert-manager.crds.yaml \
    | chart-prettier --truncate cert-manager-crds/chart/templates
