apiVersion: v1
kind: Secret
metadata:
  name: ca-key-pair
data:
  tls.crt: "{{ .Values.certificate }}"
  tls.key: "{{ .Values.key }}"
