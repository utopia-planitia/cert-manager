apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: ca-crt-publisher
spec:
  rules:
    - host: {{ .Values.domain }}
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: ca-crt-publisher
                port:
                  name: http
  tls:
    - hosts:
        - {{ .Values.domain }}
      secretName: {{ .Values.tls_secret }}
