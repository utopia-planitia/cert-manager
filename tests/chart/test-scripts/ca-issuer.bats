#!/usr/bin/env bats

cleanup() {
  kubectl -n cert-manager delete -f ca-certificate.yaml --wait=true --ignore-not-found=true
  kubectl -n cert-manager delete secret/test-certificate --wait=true --ignore-not-found=true
}

setup() {
  cleanup
}

teardown() {
  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "ca issuer creates secret" {
  run kubectl -n cert-manager apply -f ca-certificate.yaml
  [ $status -eq 0 ]
  sleep 1

  run timeout 60 sh -c "while ! kubectl -n cert-manager get secret/test-certificate; do sleep 1; done"
  [ $status -eq 0 ]
}
