#!/usr/bin/env bats

@test "certificate authority belongs to let's encrypt" {
  run sh -c "curl --fail --max-time 10 -I -k -v https://echo-server.{{ .Values.domain }}/ 2>&1"
  [[ $(echo "$output" | grep "issuer" | grep "Let's Encrypt" ) ]]
}
